/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demofxproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;

/**
 *
 * @author srala
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField textFieladAd, textFieldSoyad, textFieldAtaadi, textFieldTelefon, textFieldAdress, textFieldMaas;

    @FXML
    private DatePicker DatePickerTarix;

    @FXML
    private TableView<Person> tablePerson;

    @FXML
    private TableColumn<Person, String> verticalAd;

    @FXML
    private TableColumn<Person, String> verticalSoyad;

    private ObservableList<Person> person = FXCollections.observableArrayList();

    private final static String FILE_NAME = "/home/sralasgarov/Documents/test .xlsx";

    public void AddPersonToTable() {
        verticalAd.setCellValueFactory(new PropertyValueFactory<Person, String>("ad"));
        verticalSoyad.setCellValueFactory(new PropertyValueFactory<Person, String>("soyad"));

        try {
            FileInputStream fis = new FileInputStream(new File(FILE_NAME));
            Workbook wb = new XSSFWorkbook(fis);
            Sheet s = wb.getSheetAt(0);

            int noOfRow = s.getLastRowNum();
            System.out.println(noOfRow);
          
            for (int i = 1; i <= noOfRow; i++) {

                Person person = new Person();
                person.setAd((s.getRow(i).getCell(0)).getStringCellValue());
                person.setSoyad((s.getRow(i).getCell(1)).getStringCellValue());
                person.setAta_adi((s.getRow(i).getCell(2)).getStringCellValue());
                person.setTelefon((s.getRow(i).getCell(3)).getStringCellValue());
                person.setDogum_tarixi(s.getRow(i).getCell(4).getDateCellValue());
                System.out.println((s.getRow(i).getCell(4)).getDateCellValue()); //(s.getRow(i).getCell(4)).getNumericCellValue();
                person.setAdress((s.getRow(i).getCell(5)).getStringCellValue());
                person.setMaas((s.getRow(i).getCell(6)).getStringCellValue());

                this.person.add(person);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        tablePerson.setItems(this.person);
        tablePerson.refresh();
    }

    public void showPerson(Person person) {
        if (person != null) {
            


            
            
            
            
            System.out.println(person);
            textFieladAd.setText(person.getAd());
            textFieldSoyad.setText(person.getSoyad());
            textFieldAtaadi.setText(person.getAta_adi());
            textFieldTelefon.setText(person.getTelefon());
            LocalDate date = person.getDogum_tarixi().toInstant()
                    .atZone(ZoneId.systemDefault()).toLocalDate();
            DatePickerTarix.setValue(date);
            
           // DatePickerTarix.setValue(LocalDate.of((person.getDogum_tarixi().getYear()), (person.getDogum_tarixi().getMonth()), (person.getDogum_tarixi().getDay())));
//            System.out.println(person.getDogum_tarixi().getYear());
//            System.out.println(person.getDogum_tarixi().getMonth());
//            System.out.println(person.getDogum_tarixi().getDay());
            textFieldAdress.setText(person.getAdress());
            textFieldMaas.setText(person.getMaas());

        } else {
            textFieladAd.setText(null);
            textFieldSoyad.setText(null);
            textFieldAtaadi.setText(null);
            textFieldTelefon.setText(null);
            DatePickerTarix.setValue(null);
            textFieldAdress.setText(null);
            textFieldMaas.setText(null);

        }
    }

    public void newButtonAction() {
        textFieladAd.setText(null);
        textFieldSoyad.setText(null);
        textFieldAtaadi.setText(null);
        textFieldTelefon.setText(null);
        DatePickerTarix.setValue(null);
        textFieldAdress.setText(null);
        textFieldMaas.setText(null);
    }

    public void addButtonAction() {

        try {
            
            FileInputStream inputStream = new FileInputStream(new File(FILE_NAME));
            XSSFWorkbook wb = (XSSFWorkbook) XSSFWorkbookFactory.create(inputStream);
            Sheet sheet = wb.getSheetAt(0);
            
            
            
            
            int noOfRow = sheet.getLastRowNum();
            Row row = sheet.createRow(noOfRow+1);
            int a = sheet.getPhysicalNumberOfRows();
            int b = sheet.getTopRow();
            
            System.out.println("pyysical row--"+a);
             System.out.println("top row--"+b);
             
             System.out.println("---------------------------");
           
            
            XSSFCreationHelper creationHelper = wb.getCreationHelper();
            XSSFCellStyle cellStyle = wb.createCellStyle();
            cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyy"));
            
            
            Cell ad = row.createCell(0, CellType.STRING);
            ad.setCellValue(textFieladAd.getText()); 
           
            Cell soyad = row.createCell(1,CellType.STRING);
            soyad.setCellValue(textFieldSoyad.getText());
            
            Cell ataadi = row.createCell(2,CellType.STRING);
            ataadi.setCellValue(textFieldAtaadi.getText());
            
             Cell telefon = row.createCell(3,CellType.STRING);
            telefon.setCellValue(textFieldTelefon.getText());
            
            
            LocalDate localDate = DatePickerTarix.getValue();
            
           java.util.Date d = new SimpleDateFormat("yyyy-MM-dd").parse(localDate.toString());
           // dogum_tarixi.setCellValue(textFieldSoyad.getText());
            
            
             Cell dogum_tarixi = row.createCell(4);
             dogum_tarixi.setCellValue(d);
             dogum_tarixi.setCellStyle(cellStyle);
             
            
            
            
            
            
            
            
            
            
            
            
            Cell adress = row.createCell(5,CellType.STRING);
            adress.setCellValue(textFieldAdress.getText());
            
             Cell maas = row.createCell(6,CellType.STRING);
            maas.setCellValue(textFieldMaas.getText());
            
            
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            
            wb.write(outputStream);
            wb.close();
            outputStream.close();
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File");
        }catch(IOException e){
        e.printStackTrace();
            System.out.println("IO");} 
        catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println("Parse");
        }
    }
    
    public void editButtonAction(){
        int choose = tablePerson.getSelectionModel().getSelectedIndex();
        System.out.println(choose);
         try {
            
            FileInputStream inputStream = new FileInputStream(new File(FILE_NAME));
            XSSFWorkbook wb = (XSSFWorkbook) XSSFWorkbookFactory.create(inputStream);
            Sheet sheet = wb.getSheetAt(0);
            
            
            
            
            //int noOfRow = sheet.getLastRowNum();
            Row row = sheet.getRow(choose+1);

            
           
            
            XSSFCreationHelper creationHelper = wb.getCreationHelper();
            XSSFCellStyle cellStyle = wb.createCellStyle();
            cellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyy"));
            
            
            Cell ad = row.createCell(0, CellType.STRING);
            ad.setCellValue(textFieladAd.getText()); 
           
            Cell soyad = row.createCell(1,CellType.STRING);
            soyad.setCellValue(textFieldSoyad.getText());
            
            Cell ataadi = row.createCell(2,CellType.STRING);
            ataadi.setCellValue(textFieldAtaadi.getText());
            
             Cell telefon = row.createCell(3,CellType.STRING);
            telefon.setCellValue(textFieldTelefon.getText());
            
            
            LocalDate localDate = DatePickerTarix.getValue();
            
           java.util.Date d = new SimpleDateFormat("yyyy-MM-dd").parse(localDate.toString());
           // dogum_tarixi.setCellValue(textFieldSoyad.getText());
            
            
             Cell dogum_tarixi = row.createCell(4);
             dogum_tarixi.setCellValue(d);
             dogum_tarixi.setCellStyle(cellStyle);
             
            
            
            
            
            
            
            
            
            
            
            
            Cell adress = row.createCell(5,CellType.STRING);
            adress.setCellValue(textFieldAdress.getText());
            
             Cell maas = row.createCell(6,CellType.STRING);
            maas.setCellValue(textFieldMaas.getText());
            
            
            inputStream.close();
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            
            wb.write(outputStream);
            wb.close();
            outputStream.close();
            
             editingAddedCollection(choose);
             
            
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File");
        }catch(IOException e){
        e.printStackTrace();
            System.out.println("IO");} 
        catch (ParseException ex) {
            ex.printStackTrace();
            System.out.println("Parse");
        }
         
         
    }
    
    public void deleteButoonAction(){
          int choose = tablePerson.getSelectionModel().getSelectedIndex();
      try
        {FileInputStream inputStream = new FileInputStream(new File(FILE_NAME));
            XSSFWorkbook wb = (XSSFWorkbook) XSSFWorkbookFactory.create(inputStream);
            Sheet sheet = wb.getSheetAt(0);

          

            Row row = sheet.getRow(choose+1);

           sheet.removeRow(row);

        inputStream.close();
           
            FileOutputStream fileOut = new FileOutputStream(FILE_NAME);
            wb.write(fileOut);
            fileOut.close();
            if(choose!=-1){
            person.remove(choose);
            tablePerson.refresh();}
           
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    
    private void editingAddedCollection(int chooseIndex){
        if(chooseIndex!=-1){
        person.get(chooseIndex).setAd(textFieladAd.getText());
        person.get(chooseIndex).setSoyad(textFieldSoyad.getText());
        person.get(chooseIndex).setAta_adi(textFieldAtaadi.getText());
        person.get(chooseIndex).setTelefon(textFieldTelefon.getText());
        LocalDate localDate = DatePickerTarix.getValue();
            
           java.util.Date d;
            try {
                d = new SimpleDateFormat("yyyy-MM-dd").parse(localDate.toString());
                person.get(chooseIndex).setDogum_tarixi(d);
            } catch (ParseException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            person.get(chooseIndex).setAdress(textFieldAdress.getText());
            person.get(chooseIndex).setMaas(textFieldMaas.getText());
           // AddPersonToTable();
        tablePerson.refresh();
        }
    
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        AddPersonToTable();
        tablePerson.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showPerson(newValue)
        );
        
    }

}


