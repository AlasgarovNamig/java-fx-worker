/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demofxproject;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author srala
 */
public class Person {
    private String ad;
    private String soyad;
    private String ata_adi;
    private String telefon;
    private Date dogum_tarixi;
    private String adress;
    private String maas;


    public Person() {
    }
    
    

    public Person(String ad, String soyad, String ata_adi, String telefon, Date dogum_tarixi, String adress, String maas) {
        this.ad = ad;
        this.soyad = soyad;
        this.ata_adi = ata_adi;
        this.telefon = telefon;
        this.dogum_tarixi = dogum_tarixi;
        this.adress = adress;
        this.maas = maas;
    }


    

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getSoyad() {
        return soyad;
    }

    public void setSoyad(String soyad) {
        this.soyad = soyad;
    }

    public String getAta_adi() {
        return ata_adi;
    }

    public void setAta_adi(String ata_adi) {
        this.ata_adi = ata_adi;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Date getDogum_tarixi() {
        return dogum_tarixi;
    }

    public void setDogum_tarixi(Date dogum_tarixi) {
        this.dogum_tarixi = dogum_tarixi;
    }



    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getMaas() {
        return maas;
    }

    public void setMaas(String maas) {
        this.maas = maas;
    }

    @Override
    public String toString() {
        return "Person{" + "ad=" + ad + ", soyad=" + soyad + ", ata_adi=" + ata_adi + ", telefon=" + telefon + ", dogum_tarixi=" + dogum_tarixi + ", adress=" + adress + ", maas=" + maas + '}';
    }
    
    
    
}
